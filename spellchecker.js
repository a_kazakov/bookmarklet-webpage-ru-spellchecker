javascript: (function(){

    SHOW_SOURCE = false;

    function main() {
        var text = document.body.innerHTML;
        text = text.replace(/<.*?>/g, ' ');
        text = text.replace(/[^а-яА-ЯёЁ]/g, ' ');
        text = text.replace(/\s+/g, ' ').trim();

        var words = text.split(' ');
        words = filterWords(words);
        var fragments = splitToFragmentsByLimit(words, 10000);
        checkFragments(fragments, function(raw_corrections){
            raw_corrections     = raw_corrections.map(checkCorrections);
            var corrections     = raw_corrections.filter(function(x){return x.found;});
            var bad_corrections = raw_corrections.filter(function(x){return !x.found;});
            var bad_words       = bad_corrections.map(function(x){return x.to});
            var bw_fragments    = splitToFragmentsByLimit(bad_words, 10000);
            checkFragments(bw_fragments, function(bw_raw_corrections) {
                var bw_set = {};
                bad_words.forEach(function(x){
                    bw_set[x] = true;
                });
                bw_raw_corrections.forEach(function(x){
                    bw_set[x.word] = false;
                });
                good_corrections = bad_corrections.filter(function(x){
                    return bw_set[x.to];
                });
                corrections = corrections.concat(good_corrections);
                applyCorrections(corrections);
            });
        });
    }

    function checkCorrections(obj) {
        var desired_match;
        if (obj.word.slice(-3, obj.word.length) == "тся") {
            desired_match = obj.word.slice(0, -3) + "ться";
        } else {
            desired_match = obj.word.slice(0, -4) + "тся";
        }
        return {found: (obj.s.indexOf(desired_match) != -1), from: obj.word, to: desired_match};
    }

    function removeDuplicates(arr) {
        arr = arr.sort();
        var write_pos = 0;
        for (var i = 0; i < arr.length - 1; ++i) {
            if (arr[i] !== arr[i + 1]) {
                arr[write_pos++] = arr[i];
            }
        }
        arr[write_pos++] = arr[arr.length - 1];
        return arr.slice(0, write_pos);
    }

    function isAcceptableWord(word) {
        return word.match(/ть?ся$/) !== null;
    }

    function filterWords(words) {
        var unique = removeDuplicates(words);
        return unique.filter(isAcceptableWord);
    }

    // Max size of request is 10000 bytes.
    // Function splits big texts into fragments
    function splitToFragmentsByLimit(words, limit) {
        var fragments = [],
            fragment = [],
            fragmentLen = 0;

        for (var i = 0; i < words.length; i++) {
            var word = words[i];
            // one letter = 6 bytes: %D0%BA
            if (fragmentLen + word.length*6 > limit) {
                fragments.push(fragment.join(' '));
                fragment = [];
                fragmentLen = 0;
            }
            fragment.push(word);
            fragmentLen += word.length*6 + 3; // + space: %20 = 3 symbols
        }
        // last word
        if (fragmentLen != 0) {
            fragments.push(fragment.join(' '));
        }
        return fragments;
    }

    function checkFragments(fragments, callback) {
        queries_left = fragments.length;
        result = [];
        if (fragments.length == 0) {
            callback([]);
        }
        fragments.forEach(function(text){
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function() {
                if (this.readyState == 4) {
                    if (xhr.status == 200) {
                        result = result.concat(JSON.parse(xhr.responseText));
                        if (--queries_left == 0) {
                            callback(result);
                        }
                    } else {
                        console.log(xhr.status);
                    }
                }
            }
            xhr.open("GET", "https://speller.yandex.net/services/spellservice.json/checkText?options=7&text=" + text, true);
            xhr.send();
        });
    }

    function applyCorrections(data) {
        if (!data) return;
        var body = document.body.innerHTML;
        for (var i = 0; i < data.length; ++i) {
            var subst = data[i];
            var replacement  = '<span style="background-color:#cfc">' + subst.to   + '</span>';
            if (SHOW_SOURCE) {
                replacement += '<span style="background-color:#fcc">' + subst.from + '</span>';
            }
            var regexp = new RegExp("([^а-яА-ЯёЁ]|^)" + subst.from + "(?!=[а-яА-ЯёЁ])", "g");
            body = body.replace(regexp, "$1" + replacement);
        }
        if (document.body.innerHTML != body) {
            document.body.innerHTML = body;
        }
        alert("Replaces:" + data.length);
    }

    main();

})();
